#!/usr/bin/env python
# coding: utf-8

import model


def main():
    sample_postcode = "ox3"
    average_price_of_area = model.get_average_price_of_area(sample_postcode)
    print("The average price of this area is £" + str(average_price_of_area) + " per month.")


main()
