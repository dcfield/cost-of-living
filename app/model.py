#!/usr/bin/env python
# coding: utf-8
import utils

API_KEY = "wefrsf83datrq9grhsw2a9kw"


def get_average_price_of_area(postcode):
    rental_prices_per_month = get_rental_prices_per_month(postcode)
    average_rent_price = get_average_from_list(rental_prices_per_month)

    return average_rent_price


def get_average_from_list(list):
    total = 0
    for l in list:
        total = total + float(l.text)

    average = total / len(list)
    return average


def get_rental_prices_per_month(postcode):
    url = format_request_url(postcode)
    contents = utils.response_from_url(url)
    xml_object = utils.parse_xml_content(contents)

    rental_prices_per_month = xml_object.findall(
        'listing/rental_prices/per_month')

    return rental_prices_per_month


def format_request_url(postcode):
    request_url = "http://api.zoopla.co.uk/api/v1/property_listings.xml?" \
        + "postcode=" + postcode + "&listing_status=rent&api_key=""" + API_KEY

    return request_url
