#!/bin/bash

if [ $# -eq 0 ]
    then
        echo "Enter an argument for the commit message."
        return
fi

python -m pytest

echo "Adding pip requirements"
# pip freeze > requirements.txt

echo "Adding all files to git"
git add .

echo "Committing with message: $1"
git commit -m "$1"