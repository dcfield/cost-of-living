# Cost of living in Ireland based on location
Use this application to calculate the cost of living in a particular area in Ireland.

##Technologies used
- Python
- Daft API

## Run tests
- From root directory of project `python -m pytest`