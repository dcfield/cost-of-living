#!/usr/bin/env python
"""Tests for app/model.py"""

import pytest

import app.model as model


def setup_method(self):
    pass


def teardown_method(self):
    pass


def test_getAveragePriceOfArea_withoutAreacode_throwException():
    with pytest.raises(Exception):
        model.get_average_price_of_area("")


def test_getAveragePriceOfArea_withInvalidAreacode_throwException():
    with pytest.raises(Exception):
        model.get_average_price_of_area("12345")


def test_getAveragePriceOfArea_withValidAreacode_returnInt():
    assert isinstance(model.get_average_price_of_area("ox5"), float)

