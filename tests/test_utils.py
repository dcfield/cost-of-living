#!/usr/bin/env python
"""Tests for app/utils.py"""

import pytest
import httpretty
import urllib2

import app.utils as utils


def test_validateareacode_withBadString_throwException():
    with pytest.raises(Exception):
        utils.validate_areacode("12345")


def test_validateareacode_withEmptyString_throwException():
    with pytest.raises(Exception):
        utils.validate_areacode("")


def test_validateareacode_withIntGreaterThan4_throwException():
    with pytest.raises(Exception):
        utils.validate_areacode(456)


def test_validateareacode_withIntLessThan4_throwException():
    with pytest.raises(Exception):
        utils.validate_areacode(2)


def test_validateareacode_withValidString_returnareacode():
    areacode = "ox4"
    assert utils.validate_areacode(areacode) == areacode


@httpretty.activate
def test_responseFromUrl_withValidAreaCode_returnXmlContent():
    areacode = "ox4"
    test_url = "http://api.zoopla.co.uk/api/v1/property_listings.xml?" \
        + "postcode=" + areacode + "&listing_status=rent&api_key=1234"
    httpretty.register_uri(
        httpretty.GET, 
        test_url,
        body = "List of properties"
        )

    response = urllib2.urlopen(test_url).read()
    assert response == "List of properties"
